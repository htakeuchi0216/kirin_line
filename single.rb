require "selenium-webdriver"
require 'sqlite3'

driver = Selenium::WebDriver.for :firefox

db = SQLite3::Database.new 'line_db.db'
db.execute('select * from numbers where flg=0') do |row|
  id = row[0]
  num1 = row[1]
  num2 = row[2]

#  driver.get "https://www.k-fire.com/"
  driver.get "https://form.k-fire.com/single/"

  driver.find_element(:name,"data[serial_1]").send_key num1
  driver.find_element(:name,"data[serial_2]").send_key num2
#  driver.find_element(:name,"data[serial_1]").send_key "8005413"
#  driver.find_element(:name,"data[serial_2]").send_key "6342549"

  driver.find_element(:class,"button-submit").click

  sleep 1

  str = 'C:\kirin_line\capture\cap_' + id.to_s + '.png'
  html_str = 'C:\kirin_line\capture\html_cap_' + id.to_s + '.html'

  puts str
  driver.save_screenshot(str)

  current_url = driver.current_url
  if current_url.include?("/single/miss")
    puts "#{id} MISS"
    db.execute("update numbers set flg = 8 ,lot_date = datetime('now', 'localtime') where id = ?", id)
  elsif current_url.include?("/single/hit")
    puts "#{id} HIT !!!!!!"

    open(html_str, "w"){|f|
      f.puts driver.page_source
    }

    elements = driver.find_elements(:class_name => "u-fwBold")
    serial = ""
    elements.each do |element|
      scan = element.text.encode('UTF-8').scan(/\w{16}/)
      if !scan.empty?
          serial = scan.shift
      end
#EC3FE9F525F66537
    end
     puts "serial : #{serial}"
    db.execute("update numbers set flg = 1, tri_flg = 5 , serial = ? ,lot_date = datetime('now', 'localtime')  where id = ?", serial, id)
  else
    puts "#{id} number wrong "
    db.execute("update numbers set flg = 9,lot_date = datetime('now', 'localtime') where id = ?", id)
  end

end

driver.quit
