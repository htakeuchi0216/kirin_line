require "selenium-webdriver"
require 'sqlite3'

driver = Selenium::WebDriver.for :firefox

ids = []
idstrs = []
numbers = []
db = SQLite3::Database.new 'line_db.db'
db.execute('select * from numbers') do |row|
  id = row[0]
  num1 = row[1]
  num2 = row[2]

  ids.push(id)
  idstrs.push(id.to_s)
  numbers.push([num1,num2])

  puts "--#{id}"
  if numbers.length == 3
    puts "length == 3"
    puts "#{numbers[0][0]} , #{numbers[0][1]} "
    puts "#{numbers[1][0]} , #{numbers[1][1]} "
    puts "#{numbers[2][0]} , #{numbers[2][1]} "

    driver.get "https://form.k-fire.com/double/"

    driver.find_element(:name,"data[serial_1][code_1]").send_key numbers[0][0]
    driver.find_element(:name,"data[serial_1][code_2]").send_key numbers[0][1]

    driver.find_element(:name,"data[serial_2][code_1]").send_key numbers[1][0]
    driver.find_element(:name,"data[serial_2][code_2]").send_key numbers[1][1]

    driver.find_element(:name,"data[serial_3][code_1]").send_key numbers[2][0]
    driver.find_element(:name,"data[serial_3][code_2]").send_key numbers[2][1]

    driver.find_element(:class,"button-submit").click

    sleep 1

    
    id_str = idstrs.join("_")
    str = 'C:\kirin_line\tri_capture\cap_' + id_str + '.png'
    html_str = 'C:\kirin_line\tri_capture\html_cap_' + id_str + '.html'
    puts str
    driver.save_screenshot(str)

    current_url = driver.current_url
    st = 9 
    if current_url.include?("/double/miss")
      puts "#{id_str} MISS"
      st = 8
    elsif current_url.include?("/double/hit")
      puts "#{id_str} HIT !!!!!!"

      open(html_str, "w"){|f|
        f.puts driver.page_source
      }

#      elements = driver.find_elements(:class_name => "u-fwBold")
#
#      elements.each do |element|
#        scan = element.text.encode('UTF-8').scan(/\w{16}/)
#        if !scan.empty?
#            serial = scan.shift
#        end
#      end
#       puts "serial : #{serial}"
      st = 1
#      db.execute('update numbers set tri_flg = 1  where id = ?', id)
    else
      puts "#{id} number wrong "
      st = 9
#      db.execute('update numbers set flg = 9 where id = ?', id)
    end

    ids.each{|id|
      puts "update #{id} to status:#{st}"
      db.execute('update numbers set tri_flg = 8 where id = ?', id)
    }

    puts "numbers reset"
    numbers = []
    ids    = []
    idstrs = []

  end


end

driver.quit
